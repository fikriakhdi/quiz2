http://viima.github.io/jquery-comments orjinal Dosyaları burdan inceleyebilirsiniz. Jquery Comment üzerinden az buçuk oynamalar gerçekleştirdim.

- Düzenle Kısmını kaldırdım kişi gönderdiği yorumu düzenleyip silemez olmalıdır.
---------------------------------------------------------

Projede İstediklerim
----
1) Tüm Yorumlar Veritabanına kaydedilsin ve Onaylanmayan Yorumlar diğer kullanıcılar tarafından gösterilmesin.
2) Jquery Kısmına Ekstra [Ad Soyad] ve [Email] eklensin. Bir kere doldurulduğu takdirde Cookieye kaydedilsin kişi. Böylece aynı browserdan ikinci sefer geldiğinde ad soyad girilmiş olarak getirebiliriz. Cookie de kayıtlıysa bir daha ad soyad email girmesin. direk <span>[Ad Soyad]</span> <span>[Email]</span> olarak yazılabilir.
3) Admin cevapları için Profil resmi eklensin. (Admin Cevabı olup olmadığını veritabanında bir alanda tutulmalıdır.)
4) Yorum gönderdikten sonra "Yorumun Onay sürecinde olduğunu ve onaylandıktan sonra yayınlayacağını" bildirsin. (Onaylanmasa bile kullandığımız cookie ile yorumun o kişiye ait olup olmadığı kontrol edilsin.)
5) 1 Saat içinde yapılan tüm yorumlar yeni yorum olarak geçsin.
6) Data dosyasını iyi inceleyip AJAX ile veri çekip göndermeni istiyorum, dolayısıyla ajax veri alıp göndermeden önce MySQL veri tabanı oluşturman gerekecek; bunun için index.html içinde gönderilen örnek data yı kontrol etmeni öneririm.
7) Kod düzeni ve sadece kullanacağın kadar function yazıp açıklamaları yapmanı isterim. (PHP-MySQL bağlantısı için PDO kullanın lütfen.)
8) Herkes beğeni yapabilir fakat beğeni yapılan yorumlar cookide tutulsun.

Umarım Açıklayıcı olabilmişimdir. :)

İyi Şanslar.
