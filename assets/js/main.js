$(document).ready(function(){
    $.getJSON('api/read_message', function (data) {
        var result = jQuery.parseJSON(JSON.stringify(data))
        if (result.status == 200) {
            
            var loop = "";
            $.map( result.data, function( o, i ) {
                var template = `<li class="media">
                <a class="pull-left">
                    <img src="assets/img/avatar.png" class="img-circle profile-pic">
                </a>
                <div class="media-body">
                    <span class="text-muted pull-right">
                        <small class="text-muted">Posted at `+o.created+` By <span class="text-danger">`+o.fullname+`</span></small>
                    </span>
                    
                    <p>`+o.content+`</p>
                </div>
                </li>`;
                console.log(template)
                loop+=template
              });
            
                $("#comments_pos").html(loop)
        } else {
            alert('failed to load data')
        }
    })
})
$("#submut_message").click(function(){
    var fullname = $.cookie("fullname");
    var userId = $.cookie("userid");
    var comment = $("#content").val();
    var element = $("#fullname")
    var today = new Date(Date.now());
    var year = today.getFullYear()
    var month = today.getMonth() + 1
    var day = today.getDate()
    var hours = today.getHours()
    var minutes = today.getMinutes()
    var seconds = today.getSeconds()
    var full_date = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds
    var fullname_val = element.val()
    var template = `<li class="media">
    <a class="pull-left">
        <img src="assets/img/avatar.png" class="img-circle profile-pic">
    </a>
    <div class="media-body">
        <span class="text-muted pull-right">
            <small class="text-muted">Posted at `+full_date+` By <span class="text-danger">`+fullname_val+`</span></small>
        </span>
        
        <p>`+comment+`</p>
    </div>
    </li>`;
    console.log(template)
    if(fullname && userId){
        //send comment without registering
        $.cookie("fullname", fullname_val);
        var data = {userId:userId,content:comment}
        $.post('api/user/'+userId+"/comments",data,
            function (ret) {
                var result = JSON.parse(ret)
                if (result.status == 200) {
                    $("#content").val("")
                    setTimeout(function() {
                        $("#comments_pos").prepend(template)
                    }, 500);
                } else {
                    alert('failed to save comment');
                }
        })
    } else {
        //registering
        $.cookie("fullname", fullname_val)
        var data = {fullname:fullname_val,content:comment}
        $.post('api/register',data,
            function (ret) {
                var result = JSON.parse(ret)
                if (result.status == 200) {
                    $("#content").val("")
                    $("#fullname").attr("disabled", true)
                    $("#logoutbut").attr("disabled", false)
                    $("#logoutbut").val("Logout "+fullname_val+" ?")
                    setTimeout(function() {
                        $("#comments_pos").prepend(template)
                    }, 500);
                } else {
                    alert('failed to save comment');
                }
        })
    }
})

$("#logoutbut").click(function(){
    $.cookie("fullname", "")
    $.cookie("userid", "")
    $("#fullname").val("")
    $("#fullname").removeAttr("disabled")
    $("#logoutbut").attr("disabled", true)
    $("#logoutbut").val("Logout")
})