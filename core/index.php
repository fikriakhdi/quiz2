<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Real-Time Comeents Software By Fikri</title>
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <main role="main" class="container">

        <div class="starter-template">
            <h3>Real-Time Comments By Fikri</h3>
            <div id="danger-msg" class="alert alert-danger" style="display: none;"></div>
            <div class="form-group">
                <input type="text" id="fullname" value="<?php echo (!empty($_COOKIE['fullname'])?$_COOKIE['fullname']:"");?>" class="form-control" placeholder="Full Name" <?php echo (!empty($_COOKIE['fullname'])?"readonly":"");?>>
            </div>
            <div class="form-group">
                <textarea class="form-control" id="content" rows="2" placeholder="Comments goes here..."></textarea>
            </div>
            <div class="form-group pull-right">
                <div class="btn-group float-right" role="group" aria-label="Basic example">
                    <button type="button" id="submut_message" class="btn btn-success float-right">Submit</button>
                    <input type="button" <?php echo (!empty($_COOKIE['fullname'])?"":"disabled='true'");?>" id="logoutbut" value="<?php echo (!empty($_COOKIE['fullname'])?"Logout ".$_COOKIE['fullname']." ?":"Logout");?>" onclick="logout()" value="logout" class="btn btn-danger float-right">
                </div>
            </div>
            <div class="clearfix m-4"></div>
            <ul class="media-list">
                <div id="comments_pos">    
                </div>
            </ul>
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js"></script>
    <script>


    </script>
