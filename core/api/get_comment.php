<?php
include "core/connection.php";
$stm = $pdo->query("SELECT comments.*,user.id userId, user.full_name, user.role FROM comments JOIN user ON comments.creator =  user.id ORDER BY comments.created DESC");
$data = $stm->fetchAll();
$filtered = array();
foreach($data as $item){
    $filtered[] = array(
        'id'=>$item['id'],
        'created'=>date('Y-m-d H:i:s', strtotime($item['created'])),
        'modified'=>date('Y-m-d  H:i:s', strtotime($item['modified'])),
        'content'=>$item['content'],
        'creator'=>$item['creator'],
        'fullname'=>$item['full_name'],
    );
}
$ret = array('status'=>200, 'data'=>$filtered);
die(json_encode($ret));
?>